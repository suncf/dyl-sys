<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://dyl.com/tags" prefix="dyl" %>
<c:set var="ctx"  value="${pageContext.request.contextPath}" />
<c:set var="res"  value="${pageContext.request.contextPath}/res" />
<c:set var="version"  value="?version=1.0.0" />
