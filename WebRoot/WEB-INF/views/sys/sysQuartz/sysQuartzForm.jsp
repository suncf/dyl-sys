<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form class="layui-form" action="${not empty sysQuartz?'sysQuartz!update.do':'sysQuartz!add.do'}"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="id" value="${sysQuartz.id}" />
	<div class="layui-form-item">
		<label class="layui-form-label">触发器名称</label>
		<div class="layui-input-inline">
		  <input type="text" name="triggername" required  lay-verify="required" placeholder="请输入触发器名称" value="${sysQuartz.triggername}" class="layui-input"  />
		</div>
    </div>
	<div class="layui-form-item">
		<label class="layui-form-label">时间表达式</label>
		<div class="layui-input-inline">
		  <input type="text" name="cronexpression" required  lay-verify="required" placeholder="请输入时间表达式" value="${sysQuartz.cronexpression}" class="layui-input"  />
		</div>
    </div>
	<div class="layui-form-item">
		<label class="layui-form-label">脚本名称</label>
		<div class="layui-input-inline">
		  <input type="text" name="jobdetailname" required  lay-verify="required" placeholder="请输入脚本名称" value="${sysQuartz.jobdetailname}" class="layui-input"  />
		</div>
    </div>
	<div class="layui-form-item">
		<label class="layui-form-label">目标类</label>
		<div class="layui-input-inline">
		  <input type="text" name="targetobject" required  lay-verify="required" placeholder="请输入目标类" value="${sysQuartz.targetobject}" class="layui-input"  />
		</div>
    </div>
	<div class="layui-form-item">
		<label class="layui-form-label">方法名</label>
		<div class="layui-input-inline">
		  <input type="text" name="methodname" required  lay-verify="required" placeholder="请输入方法名" value="${sysQuartz.methodname}" class="layui-input"  />
		</div>
    </div>
    <div class="layui-form-item">
	    <label class="layui-form-label">是否并发启动任务</label>
	    <div class="layui-input-block">
	      <input type="radio" name="concurrent" value="1" title="是" ${sysQuartz.concurrent ==1?'checked':''}  ${empty sysQuartz?'checked':''} />
	      <input type="radio" name="concurrent" value="0" title="否" ${sysQuartz.concurrent ==0?'checked':''} />
	    </div>
	 </div>
    
	<div class="layui-form-item">
		<label class="layui-form-label">状态</label>
		<div class="layui-input-block">
	      <input type="radio" name="state" value="1" title="启用" ${sysQuartz.state ==1?'checked':''}   />
	      <input type="radio" name="state" value="0" title="不启用" ${sysQuartz.state ==0?'checked':''} ${empty sysQuartz?'checked':''} />
	    </div>
    </div>
  <!--隐藏提交按钮，用于触发表单校验 -->
  <button lay-filter="saveOrUpdate" lay-submit class="layui-hide" />
</form>
<script>
	form.render();//重新渲染表单
	form.on('submit(saveOrUpdate)', function(data){//表单提交方法
 		getJsonByajaxForm("saveOrUpdateForm","",function(data){
			if(data.result){
				layer.closeAll('page'); //关闭信息框
				layer.alert($('.layui-layer-btn0').text()+"成功!",function(){
					$('#search').click();//查询
				}); 
			}else{
				l.alert(data.msg);//错误消息弹出
			}
		},true); 
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
	}); 
</script>
