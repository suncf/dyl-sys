package dyl.sys.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import dyl.common.exception.CommonException;
import dyl.common.util.ConfigUtil;

/**
 * <DL>
 * <DT><B> 类名称:文件处理工具类 </B></DT>
 * <p>
 * <DT><B> 类说明: 对磁盘文件进行属性或物理操作 </B></DT>
 * </DL>
 * <p>
 * @author Daniel
 * @version 1.00, 2014-7-28
 */
public class FileUtils {
	static Logger logger = Logger.getLogger(FileUtils.class);  
	private static String savePath = ConfigUtil.getValue("filePath");
	
	/**
	 * <br>
	 * <b>方法名称: 磁盘文件名处理</b><br>
	 * <b>方法说明: 根据传入的文件名构造磁盘文件名称(32位平台编号+15位日期yyyymmddhhmmsss+.后缀名)</b><br>
	 * <br>
	 * @param srcDiskName 源文件名
	 * @return
	 * @throws CommonException
	 */
	public static String diskNameProductor(String ebpNo, String srcDiskName) throws CommonException {
		try {
			String fileType = fileTypeProductor(srcDiskName);
			String prefixFileName = DateUtil.getYyyyMMddHHmm() + String.valueOf((int)(Math.random()*900)+100) ;
			StringBuffer str = new StringBuffer();
			str.append(ebpNo);
			str.append(prefixFileName);
			str.append(".");
			str.append(fileType);
			return str.toString();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
	}
	
	/**
	 * <br>
	 * <b>方法名称: 磁盘文件名处理</b><br>
	 * <b>方法说明: 根据传入的文件名构造磁盘文件名称(15位日期yyyymmddhhmmsss+.后缀名)</b><br>
	 * <br>
	 * @param srcDiskName 源文件名
	 * @return
	 * @throws CommonException
	 */
	public static String diskNameProductorNoSuffix() throws CommonException {
		try {
			String prefixFileName = DateUtil.getYyyyMMddHHmm() + String.valueOf((int)(Math.random()*900)+100) ;
			StringBuffer str = new StringBuffer();
			str.append(prefixFileName);
			return str.toString();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
	}
	
	/**
	 * <br>
	 * <b>方法名称: 磁盘文件名处理</b><br>
	 * <b>方法说明: 根据传入的文件名构造磁盘文件名称(15位日期yyyymmddhhmmsss+.后缀名)</b><br>
	 * <br>
	 * @param srcDiskName 源文件名
	 * @return
	 * @throws CommonException
	 */
	public static String diskNameProductor(String srcDiskName) throws CommonException {
		try {
			String fileType = fileTypeProductor(srcDiskName);
			String prefixFileName = DateUtil.getYyyyMMddHHmm() + String.valueOf((int)(Math.random()*900)+100) ;
			StringBuffer str = new StringBuffer();
			str.append(prefixFileName);
			str.append(".");
			str.append(fileType);
			return str.toString();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
	}
	
	/**
	 * <br>
	 * <b>方法名称: 文件类型处理</b><br>
	 * <b>方法说明: 根据传入的文件名构造类型名</b><br>
	 * <br>
	 * @param srcDiskName 源文件名
	 * @return
	 * @throws CommonException
	 */
	public static String fileTypeProductor(String srcDiskName) throws CommonException {
		try {
			int i = StringUtils.lastIndexOf(srcDiskName, ".") + 1;
			int j = srcDiskName.length();
			String fileType = StringUtils.substring(srcDiskName, i,j);
			return fileType.toLowerCase();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
	}
	
	/**
	 * <br>
	 * <b>方法名称: 文件路径处理</b><br>
	 * <b>方法说明: 根据传入的文件名构造文件全路径</b><br>
	 * <br>
	 * @param srcDiskName 源文件名
	 * @return
	 * @throws CommonException
	 */
	public static String filePathProductor(String srcDiskName) throws CommonException {
		try {
			String subSavePath = StringUtils.substring(srcDiskName, 0,6);
			StringBuffer str = new StringBuffer();
			str.append(savePath);
			str.append(File.separator);
			str.append(subSavePath);
			str.append(File.separator);
			str.append(srcDiskName);
			return str.toString();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
	}
	
	/**
	 * <br>
	 * <b>方法名称: 文件检查</b><br>
	 * <b>方法说明: 检查传入的文件路径,若不存在则创建</b><br>
	 * <br>
	 * @return
	 * @throws CommonException
	 */
	public static boolean fileMaker(String srcDiskName) throws CommonException {
		boolean b = false;
		try {
			File f = new File(srcDiskName);
			if(!f.exists()){
				b = f.createNewFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
		return b;
	}
	
	/**
	 * <br>
	 * <b>方法名称: 文件夹路径检查</b><br>
	 * <b>方法说明: 检查传入的文件夹路径,若不存在则创建</b><br>
	 * <br>
	 * @return
	 * @throws CommonException
	 */
	public static boolean filePathMaker() throws CommonException {
		boolean b = false;
		try {
			File f = new File(savePath + DateUtil.getYyyyMm());
			if(!f.isDirectory()&&!f.exists()){
				b = f.mkdirs();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
		return b;
	}

	/**
	 * <br>
	 * <b>方法名称: 文件上传</b><br>
	 * <b>方法说明: 根据控件对象，路径及文件属性上传文件</b><br>
	 * <br>
	 * @param file 控件对象
	 * @param fileFileName 文件属性名
	 * @throws CommonException
	 */
	public static void fileUpload(File file, String fileFileName) throws CommonException {
		InputStream in = null;
		OutputStream out = null;
		try {
			filePathMaker();
			String path = filePathProductor(fileFileName);
			in = new FileInputStream(file);
			File uploadFile = new File(path);
			out = new FileOutputStream(uploadFile);
			byte[] buffer = new byte[1024 * 1024];
			int length;
			while ((length = in.read(buffer)) != -1) {
				out.write(buffer, 0, length);
			}
			in.close();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		} finally {
			try {
				if (null != in) {
					in.close();
				}
				if (null != out) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				logger.error(e.getMessage(), e);
				throw new CommonException(e);
			}
		}
	}
	
	/**
	 * <br>
	 * <b>方法名称: 文件删除</b><br>
	 * <b>方法说明: 根据磁盘路径、磁盘文件名删除文件</b><br>
	 * <br>
	 * @param diskName 文件名称
	 * @throws CommonException
	 */
	public static void deleteFile(String diskName) {
		try {
			String path = FileUtils.filePathProductor(diskName);
			File f = new File(path);
			if(f.isFile() && f.exists()){
				f.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
	}
	
	/**
	 * <br>
	 * <b>方法名称: 文件是否存在</b><br>
	 * <b>方法说明: 根据磁盘路径、磁盘文件名删除文件</b><br>
	 * <br>
	 * @param diskName 文件名称
	 * @throws CommonException
	 */
	public static void existsFile(String diskName) {
		try {
			logger.info(diskName);
			
			String path = FileUtils.filePathProductor(diskName);
			logger.info("文件全路径:"+path);
			
			File f = new File(path);
			if(!f.isFile() || !f.exists()){
				//throw new FileNotFoundException();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		}
	}
	
	public static void createTextFile(String path, String text){
		PrintStream ps = null;
		FileOutputStream fos = null;
		try {
			FileUtils.filePathMaker();
			fos = new FileOutputStream(new File(path));
	         ps = new PrintStream(fos);
	         ps.println(text);// 往文件里写入字符串
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new CommonException(e);
		} finally {
			try {
				if(null!=ps){
					ps.close();
				}
				if(null!=fos){
					fos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				logger.error(e.getMessage(), e);
				throw new CommonException(e);
			}
		}
	}
	
	//通过realname获取文件file对象
	public static File getFileByName(String realname){
		return new File(FileUtils.filePathProductor(realname));
	}
}
