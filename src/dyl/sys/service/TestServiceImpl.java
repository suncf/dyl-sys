package dyl.sys.service;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.Test;
/**

 * @author Dyl
 * 2017-06-16 16:51:55
 */
@Service
@Transactional
public class TestServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	/**
	 * 说明：分页查询表t_test记录封装成List集合
	 * @return List<Test
	 */
	public List<Test>  findTestList(Page page,Test test) throws Exception{
		String sql = "select id,test1,yrdy2,test3 from t_test t where 1=1";
		List<Object> con = new ArrayList<Object>();
		if(test.getId()!=null){
			sql+=" and t.id=?";
			con.add(test.getId());
		}
		if(StringUtils.isNotEmpty(test.getTest1())){
			sql+=" and t.test1 like ?";
			con.add("%"+test.getTest1()+"%");
		}
		if(StringUtils.isNotEmpty(test.getYrdy2())){
			sql+=" and t.yrdy2 like ?";
			con.add("%"+test.getYrdy2()+"%");
		}
		if(StringUtils.isNotEmpty(test.getTest3())){
			sql+=" and t.test3 like ?";
			con.add("%"+test.getTest3()+"%");
		}
		List<Test> testList =  jdbcTemplate.queryForListBeanByPage(sql, con, Test.class, page);
		
		return testList;
	}
	/**
	 * 说明：根据主键查询表t_test中的一条记录 
	 * @return Test
	 */
	public Test getTest(Integer  id) throws Exception{
		String sql = "select id,test1,yrdy2,test3 from t_test where id=?";
		Test test  =  jdbcTemplate.queryForBean(sql, new Object[]{id},Test.class); 
		return test;
	}
	/**
	 * 说明：往表t_test中插入一条记录
	 * @return int >0代表操作成功
	 */
	public int insertTest(Test test) throws Exception{
		String sql = "insert into t_test(id,test1,yrdy2,test3) values("+DylSqlUtil.getnextSeqNextVal()+",?,?,?)";
		int returnVal=jdbcTemplate.update(sql,new Object[]{test.getTest1(),test.getYrdy2(),test.getTest3()});
		return returnVal;
	}
	/**
	 * 说明：根据主键更新表t_test中的记录
	 * @return int >0代表操作成功
	 */
	public int updateTest(Test test) throws Exception{
		String sql = "update t_test t set ";
		List<Object> con = new ArrayList<Object>();
		if(test.getTest1()!=null){
			sql+="t.test1=?,";
			con.add(test.getTest1());
		}
		if(test.getYrdy2()!=null){
			sql+="t.yrdy2=?,";
			con.add(test.getYrdy2());
		}
		if(test.getTest3()!=null){
			sql+="t.test3=?,";
			con.add(test.getTest3());
		}
		sql=sql.substring(0,sql.length()-1);
		sql+=" where id=?";
		con.add(test.getId());
		int returnVal=jdbcTemplate.update(sql,con.toArray());
		return returnVal;
	}
	/**
	 * 说明：根据主键删除表t_test中的记录
	 * @return int >0代表操作成功
	 */
	public int[] deleteTest(String  ids) throws Exception{
		String sql = "delete from  t_test where id=?";
		String[] idArr  =  ids.split(",");
		List<Object[]> paraList = new ArrayList<Object[]>(); 
		for (int i = 0; i < idArr.length; i++) {
			paraList.add(new Object[]{idArr[i]});
		}
		return jdbcTemplate.batchUpdate(sql,paraList);
	}
}
